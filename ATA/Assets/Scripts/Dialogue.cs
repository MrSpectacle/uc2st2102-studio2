﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Dialogue : MonoBehaviour
{
    public string questOne;
    public string questTwo;
    [TextArea(15, 20)]
    public string questThree;
    public string questionOne;
    public string questionTwo;
    public string giveMedicine;
    public string giveWaterFiltration;
    [TextArea(15, 20)]
    public string foundFamily;

    //button text
    public string giveWaterFiltrationText;
    public string enterCampText;
    public string goToMenuText;

    public GameObject questTextBox;
    public GameObject questText;
    public GameObject textBox;
    public GameObject notEnoughEnergyText;
    public GameObject leaveButton;
    public Button giveObjectButton;
    public TextMeshProUGUI giveObjectButtonText;
    public TextMeshProUGUI textDisplay;

    bool finishedQuestOne = false;
    bool finishedQuestTwo = false;
    bool finishedGame = false;

    Inventory inventory;
    MainQuest mainQuest;

    private void Start()
    {
        inventory = GameObject.Find("GameBehavior").GetComponent<Inventory>();
        mainQuest = GameObject.Find("GameBehavior").GetComponent<MainQuest>();
    }


   // public void QuestPartOne()
   // {
   //     if (finishedQuestOne == false)
   //     {
   //             textDisplay.text = questOne;
   //     }
   //}

   // public void QuestPartTwo()
   // {
   //     if (finishedQuestOne == true && finishedQuestTwo == false)
   //     {
   //        giveObjectButtonText.text = giveWaterFiltrationText.ToString();
   //        textDisplay.text = questTwo;
   //    }
   //}

    public void QuestPartThree()
    {

        giveObjectButtonText.text = enterCampText.ToString();
        textDisplay.text = questThree;
    }

    public void QuestionOne()
    {
        textDisplay.text = questionOne;
    }

    public void QuestionTwo()
    {
        textDisplay.text = questionTwo;
    }

    //public void GiveMedicine()
    //{
    //    if (inventory.totalMedicineAmount >= 1 && finishedQuestOne == false)
    //    {
    //        textDisplay.text = giveMedicine;
    //        inventory.totalMedicineAmount -= 1;
    //       inventory.medicineAmountText.text = inventory.totalMedicineAmount.ToString();
    //        finishedQuestOne = true;
    //        giveObjectButton.interactable = false;
    //    }
    //}

    // public void GiveWaterFiltration()
    // {
    //     if (inventory.hasWaterFiltration == true && finishedQuestOne == true)
    //     {
    //         textDisplay.text = giveWaterFiltration; 
    //         inventory.hasWaterFiltration = false;
    //         finishedQuestTwo = true;
    //         giveObjectButton.interactable = false;
    //     }
    // }


    public void GoToMenu()
    {
        if (finishedGame == true)
        {
            SceneManager.LoadScene(0);
        }
    }

    public void EnterCamp()
    {
        if (inventory.hasWeapon == true && inventory.hasArmor == true && inventory.totalFoodAmount >= 5 && inventory.totalMedicineAmount >= 2)
        {
            textDisplay.text = foundFamily;
            giveObjectButtonText.text = goToMenuText.ToString();
            leaveButton.SetActive(false);
            finishedGame = true;
            
        }
    }

    public void Leave()
    {
        textBox.SetActive(false);
        notEnoughEnergyText.SetActive(false);
        questTextBox.SetActive(false);
        questText.SetActive(false);
        giveObjectButton.interactable = true;
    }
}