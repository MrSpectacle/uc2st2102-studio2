﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class SleepSystem : MonoBehaviour
{
    public GameObject sleepPanel;
    public GameObject gameOverPanel;
    public GameObject fade;
    public GameObject bedOutline;
    public GameObject sleepUI;

    public bool isPanelOpen = false;
    public bool gameOver = false;

    public TextMeshProUGUI energyAmountText;
    public TextMeshProUGUI hungerAmountText;
    public TextMeshProUGUI healthAmountText;

    public int energy = 100;
    public int hunger = 100;
    public int health = 100;

    DailyLog dailyLog;
    Animator anim;

    private void Start()
    {
        sleepPanel.SetActive(false);
        gameOverPanel.SetActive(false);
        energyAmountText.text = energy.ToString();
        hungerAmountText.text = hunger.ToString();
        healthAmountText.text = health.ToString();
        anim = fade.GetComponent<Animator>();
        dailyLog = GameObject.Find("Daily Log").GetComponent<DailyLog>();
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            CloseTextBox();
        }

        if (health <= 0)
        {
            sleepPanel.SetActive(true);
            isPanelOpen = true;
            gameOverPanel.SetActive(true);
            gameOver = true;
        }
    }

    //private void OnMouseDown()
    //{
    //    GameObject door = GameObject.Find("Door");
    //    GameObject food = GameObject.Find("pCylinder1");
    //    GameObject craftingSystem = GameObject.Find("Cardboard boxes");

    //    if (door.GetComponent<ExpeditionSystem>().isPanelOpen == false && food.GetComponent<Food>().isPanelOpen == false && craftingSystem.GetComponent<CraftingSystem>().isPanelOpen == false)
    //    {
    //        sleepPanel.SetActive(true);
    //        isPanelOpen = true;
    //    }
    //}

    public void GoToSleep()
    {
        dailyLog.isPanelOpen = true;

        if (hunger <= 0)
        {
            gameOverPanel.SetActive(true);
            gameOver = true;
            sleepUI.SetActive(false);
        }

        if (hunger > 0)
        {
            anim.SetBool("goToSleep", true);
            energy = 100;
            hunger = hunger - 20;
            sleepPanel.SetActive(false);
            isPanelOpen = false;
            energyAmountText.text = energy.ToString();
            hungerAmountText.text = hunger.ToString();
            Cursor.lockState = CursorLockMode.Locked;
            Invoke("hasSlept", 195f * Time.deltaTime);
        }

        if (hunger < 0)
        {
            hunger = 0;
            hungerAmountText.text = hunger.ToString();
        }  
    }

    public void CloseTextBox()
    {
        sleepPanel.SetActive(false);
        isPanelOpen = false;
    }

    void hasSlept()
    {
        anim.SetBool("goToSleep", false);
        dailyLog.NewDay();
        Cursor.lockState = CursorLockMode.None;
    }

    private void OnMouseEnter()
    {
        GameObject door = GameObject.Find("Door");
        GameObject food = GameObject.Find("pCylinder1");
        GameObject craftingSystem = GameObject.Find("Cardboard boxes");

        if (dailyLog.isPanelOpen == false && door.GetComponent<ExpeditionSystem>().isPanelOpen == false && food.GetComponent<Food>().isPanelOpen == false && craftingSystem.GetComponent<CraftingSystem>().isPanelOpen == false && gameOver == false)
        {
            sleepUI.SetActive(true);
        }
    }

    private void OnMouseExit()
    {
        sleepUI.SetActive(false);
    }
}
