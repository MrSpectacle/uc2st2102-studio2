﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MainQuest : MonoBehaviour
{
    public GameObject textBox;
    public GameObject questPartOne;
    public GameObject notEnoughEnergyText;
    public GameObject dontHaveObjectText;
    public GameObject questTextBox;
    public GameObject questText;

    SleepSystem sleepSystem;
    Inventory inventory;

    private void Start()
    {
        sleepSystem = GameObject.Find("Bed").GetComponent<SleepSystem>();
        inventory = GameObject.Find("GameBehavior").GetComponent<Inventory>();
    }

    public void CheckEnergy()
    {
        if (sleepSystem.energy < 100)
        {
            textBox.SetActive(true);
            notEnoughEnergyText.SetActive(true);
        }

        if (sleepSystem.energy >= 100)
        {
            questTextBox.SetActive(true);
            questText.SetActive(true);
            sleepSystem.energy -= 100;
            sleepSystem.energyAmountText.text = sleepSystem.energy.ToString();
        }
    }

    private void DontHaveObject()
    {
        textBox.SetActive(true);
        dontHaveObjectText.SetActive(true);
    }
}