﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Explore : MonoBehaviour
{
    public GameObject groceryStore;
    public GameObject groceryStoreExploreButton;
    public GameObject drugstore;
    public GameObject drugstoreExploreButton;
    public GameObject hardwareStore;
    public GameObject hardwareStoreExploreButton;
    public GameObject recieveFoodText;
    public GameObject recieveMedicineText;
    public GameObject recieveToolsText;

    SleepSystem sleepSystem;

    private void Start()
    {
        sleepSystem = GameObject.Find("Bed").GetComponent<SleepSystem>();
    }


    public void ExploreGroceryStore()
    {
        if (sleepSystem.energy >= 50)
        {
            groceryStore.SetActive(true);
            groceryStoreExploreButton.SetActive(false);
            sleepSystem.energy -= 50;
            sleepSystem.energyAmountText.text = sleepSystem.energy.ToString();
        }
    }

    public void ExploreDrugstore()
    {
        if (sleepSystem.energy >= 50)
        {
            drugstore.SetActive(true);
            drugstoreExploreButton.SetActive(false);
            sleepSystem.energy -= 50;
            sleepSystem.energyAmountText.text = sleepSystem.energy.ToString();
        }
    }

    public void ExplorHardwareStore()
    {
        if (sleepSystem.energy >= 50)
        {
            hardwareStore.SetActive(true);
            hardwareStoreExploreButton.SetActive(false);
            sleepSystem.energy -= 50;
            sleepSystem.energyAmountText.text = sleepSystem.energy.ToString();
        }

    }

    public void ShowRecieveFoodText()
    {
        recieveFoodText.SetActive(true);
    }

    public void RemoveRecieveFoodText()
    {
        recieveFoodText.SetActive(false);
    }

    public void ShowRecieveMedicineText()
    {
        recieveMedicineText.SetActive(true);
    }

    public void RemoveRecieveMedicineText()
    {
        recieveMedicineText.SetActive(false);
    }

    public void ShowRecieveToolsText()
    {
        recieveToolsText.SetActive(true);
    }

    public void RemoveRecieveToolsText()
    {
        recieveToolsText.SetActive(false);
    }
}
