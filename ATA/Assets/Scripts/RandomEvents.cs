﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class RandomEvents : MonoBehaviour
{
    public string eventOne;
    public string eventOneAccepted;
    public string eventOneQuestionOneText;
    public string eventOneQuestionTwoText;

    public string eventTwo;
    public string eventTwoAccepted;
    public string eventTwoAcceptedTwice;

    public string eventThree;
    public string eventThreeAccepted;
    public string eventThreeAcceptedTwice;
    public string eventThreeFailed;
    public string eventThreeGiveUp;

    public string eventFour;
    public string eventFourAccepted;
    public string eventFourFirst;
    public string eventFourAcceptedFirst;
    public string eventFourAcceptedTwice;

    public string eventFive;
    public string eventFiveAccepted;
    public string eventFiveGiveUp;

    public string eventSix;
    public string eventSixAccepted;

    public string eventSeven;
    public string eventSevenAccepted;
    [TextArea(15, 20)]
    public string eventSevenFoundStash;

    public string eventEight;
    public string eventEightAccepted;

    public string resetLeaveButtonText;

    public string eventOneButtonEmptyText;
    public string eventOneAcceptButtonText;
    public string eventTwoAcceptButtonText;
    public string eventThreeAcceptButtonText;
    public string eventThreeLeaveButtonText;
    public string eventFourAcceptButtonText;
    public string eventFourNeedHelpButtonText;
    public string eventFourGiveMedicineButtonText;
    public string eventFiveAcceptButtonText;
    public string eventFiveLeaveButtonText;
    public string eventSixAcceptButtonText;
    public string eventSevenAcceptButtonText;
    public string eventEightAcceptButtonText;
    public string eventEightLeaveButtonText;

    public string eventOneButtonQuestionOneText;
    public string eventOneButtonQuestionTwoText;

    public string questHintOne;
    public string questHintTwo;
    public string questHintThree;
    public string questHintFour;
    public string questHintFive;
    public string hintLogEnding;

    public TextMeshProUGUI textDisplay;
    public Button firstButton;
    public TextMeshProUGUI firstButtonText;
    public Button acceptButton;
    public TextMeshProUGUI acceptButtonText;
    public TextMeshProUGUI leaveButtonText;
    public TextMeshProUGUI hintText;
    public TextMeshProUGUI needText;

    public GameObject randomEventText;
    public GameObject textBox;
    public GameObject RandomEventFirstButton;
    public GameObject RandomEventOkayButton;
    public GameObject RandomEventRefuseButton;
    public GameObject okayButton;
    public GameObject sanctuaryLocation;

    public bool randomEventCheck = false;
    public bool notEnoughEnergy = false;
    public bool eventOneStarted = false;

    public bool eventOneCompleted = false;
    public bool eventTwoCompleted = false;
    public bool eventThreeCompleted = false;
    public bool eventFourTalked = false;
    public bool eventFourCompleted = false;
    public bool askedIfNeedHelp = false;
    public bool eventSixCompleted = false;

    public bool gaveFood = false;
    public bool didNotGiveFood = false;
    public bool exploredBuilding = false;
    public bool gaveMedicine;
    public bool attacked = false;
    public bool failedAttack = false;
    public bool foundNoteBook = false;
    public bool lockBroken = false;
    public bool lockFixed = false;

    private int random = 0;
    public int eventNumber = 0;
    private int dogRandom = 0;
    public int daysSinceEventDog = 0;

    public bool hintLogFinished = false;

    public int questHints = 0;
    public int lockBrokenDays = 0;
    public int lockNotBrokenDays = 0;

    Inventory inventory;
    SleepSystem sleepSystem;
    ExpeditionSystem expeditionSystem;

    public int noEvents = 0;


    private void Start()
    {
        inventory = GameObject.Find("GameBehavior").GetComponent<Inventory>();
        sleepSystem = GameObject.Find("Bed").GetComponent<SleepSystem>();
        expeditionSystem = GameObject.Find("Door").GetComponent<ExpeditionSystem>();
    }

    private void Update()
    {
        if (questHints >= 5 && hintLogFinished == false)
        {
            hintText.text = hintText.text + ("\n") + hintLogEnding;
            sanctuaryLocation.SetActive(true);
            hintLogFinished = true;
        }
    }

    public void RandomEvent()
    {
        random = Random.Range(0, 3);

        if (random != 1)
        {
            noEvents++;
        }

        if (noEvents >= 3)
        {
            random = 1;
        }

        if (random == 1 && noEvents >= 1)
        {
            FindEvent();
        }
    }

    private void FindEvent()
    {
        noEvents = 0;
        eventNumber = Random.Range(0, 8);

        if (eventNumber == 0)
        {
            if (eventOneCompleted == false)
            {
                acceptButtonText.text = eventOneAcceptButtonText.ToString();
                textDisplay.text = eventOne;
                needText.text = "(Need 1 food)";

            }
            else
            {
                FindEvent();
            }
        }

        if (eventNumber == 1)
        {
            acceptButtonText.text = eventTwoAcceptButtonText.ToString();
            textDisplay.text = eventTwo;
            needText.text = "(Need 50 energy)";
        }

        if (eventNumber == 2)
        {
            if (eventOneCompleted == true)
            {
                acceptButtonText.text = eventThreeAcceptButtonText.ToString();
                leaveButtonText.text = eventThreeLeaveButtonText.ToString();
                textDisplay.text = eventThree;
            }
            else
            {
                FindEvent();
            }
        }

        if (eventNumber == 3)
        {
            acceptButtonText.text = eventFourAcceptButtonText.ToString();
            textDisplay.text = eventFour;
        }

        if (eventNumber == 4)
        {
            FindEvent();
        }

        if (eventNumber == 5)
        {
            if (eventOneCompleted == true && eventSixCompleted == false)
            {
                acceptButtonText.text = eventSixAcceptButtonText.ToString();
                textDisplay.text = eventSix;
                needText.text = "(Need 1 medicine)";
            }
            else
            {
                FindEvent();
            }
        }

        if (eventNumber == 6)
        {
            if (daysSinceEventDog >= 2)
            {
                acceptButtonText.text = eventSevenAcceptButtonText.ToString();
                textDisplay.text = eventSeven;
                dogRandom = Random.Range(0, 2);
                daysSinceEventDog = 0;
            }
            else
            {
                FindEvent();
            }
        }

        if (eventNumber == 7)
        {
            if (lockBroken == false && lockNotBrokenDays >= 8)
            {
                acceptButtonText.text = eventEightAcceptButtonText.ToString();
                leaveButtonText.text = eventEightLeaveButtonText.ToString();
                textDisplay.text = eventEight;
                needText.text = "(Need 5 tools)";
            }
            else
            {
                FindEvent();
            }
        }

        textBox.SetActive(true);
        randomEventText.SetActive(true);
        RandomEventFirstButton.SetActive(true);
        RandomEventOkayButton.SetActive(true);
        RandomEventRefuseButton.SetActive(true);
        okayButton.SetActive(false);
        randomEventCheck = true;

        if (eventNumber != 6)
        {
            daysSinceEventDog++;
        }
    }

    public void Leave()
    {
        if (leaveButtonText.text == resetLeaveButtonText.ToString() || leaveButtonText.text == eventEightLeaveButtonText.ToString())
        {
            textBox.SetActive(false);
            randomEventText.SetActive(false);
            randomEventCheck = false;
            RandomEventFirstButton.SetActive(false);
            RandomEventOkayButton.SetActive(false);
            RandomEventRefuseButton.SetActive(false);
            okayButton.SetActive(true);
            firstButtonText.text = eventOneButtonEmptyText.ToString();
            needText.text = " ";
            if (gaveMedicine == true)
            {
                eventFourCompleted = true;
            }
            eventFourTalked = false;
        }

        if (eventNumber == 0)
        {
            didNotGiveFood = true;

            if (eventOneStarted == true)
            {
                didNotGiveFood = false;
            }

            eventOneStarted = false;
            lockFixed = false;
        }
    }

    public void eventOneGiveFood()
    {
        if (eventNumber == 0 && eventOneStarted == false && inventory.totalFoodAmount >= 1)
        {
            needText.text = " ";
            eventOneStarted = true;
            textDisplay.text = eventOneAccepted;
            firstButtonText.text = eventOneButtonQuestionOneText.ToString();
            acceptButtonText.text = eventOneButtonQuestionTwoText.ToString();
            gaveFood = true;
            inventory.totalFoodAmount--;
            inventory.foodAmountText.text = inventory.totalFoodAmount.ToString();
            questHints++;
            if (questHints <= 5)
            {
                hintText.text = hintText.text + ("\n") + questHintOne;
            }
            eventOneCompleted = true;
        }
    }

    public void eventOneQuestionOne()
    {
        if (eventOneStarted == true)
        {
            textDisplay.text = eventOneQuestionOneText;
        }
    }

    public void eventOneQuestionTwo()
    {
        if (eventOneStarted == true)
        {
            textDisplay.text = eventOneQuestionTwoText;
        }
    }

    public void eventTwoExploreBuilding()
    {
        if (eventNumber == 1 && sleepSystem.energy >= 50)
        {
            needText.text = " ";
            acceptButtonText.text = eventOneButtonEmptyText.ToString();
            if (eventTwoCompleted == true)
            {
                textDisplay.text = eventTwoAcceptedTwice;
                inventory.totalFoodAmount++;
                inventory.foodAmountText.text = inventory.totalFoodAmount.ToString();
            }
            if (eventTwoCompleted == false)
            {
                textDisplay.text = eventTwoAccepted;
                eventTwoCompleted = true;
                exploredBuilding = true;
                questHints++;
                if (questHints <= 5)
                {
                    hintText.text = hintText.text + ("\n") + questHintTwo;
                }
            }
            sleepSystem.energy -= 50;
            sleepSystem.energyAmountText.text = sleepSystem.energy.ToString();
        }
    }

    public void eventThreeAttack()
    {
        if (eventNumber == 2 && inventory.hasWeapon == true)
        {
            if (eventThreeCompleted == true)
            {
                textDisplay.text = eventThreeAcceptedTwice;
            }

            if (eventThreeCompleted == false)
            {
                textDisplay.text = eventThreeAccepted;
                if (questHints <= 5)
                {
                    questHints++;
                    hintText.text = hintText.text + ("\n") + questHintThree;
                }
                attacked = true;
                eventThreeCompleted = true;
            }

            acceptButtonText.text = eventOneButtonEmptyText.ToString();
            leaveButtonText.text = resetLeaveButtonText.ToString();
        }
    }

    public void EventThreeFailed()
    {
        if (eventNumber == 2 && inventory.hasWeapon == false)
        {
            acceptButtonText.text = eventOneButtonEmptyText.ToString();
            leaveButtonText.text = resetLeaveButtonText.ToString();
            textDisplay.text = eventThreeFailed;
            sleepSystem.health -= 50;
            sleepSystem.healthAmountText.text = sleepSystem.health.ToString();
            LoseResources();
            failedAttack = true;
        }
    }

    public void EventThreeGiveUp()
    {
        if (eventNumber == 2 && leaveButtonText.text != resetLeaveButtonText.ToString())
        {
            acceptButtonText.text = eventOneButtonEmptyText.ToString();
            leaveButtonText.text = resetLeaveButtonText.ToString();
            textDisplay.text = eventThreeGiveUp;
            LoseResources();
        }
    }

    private void LoseResources()
    {
        if (inventory.totalFoodAmount >= 1)
        {
            inventory.totalFoodAmount --;
            inventory.foodAmountText.text = inventory.totalFoodAmount.ToString();
        }

        if (inventory.totalMedicineAmount >= 1)
        {
            inventory.totalMedicineAmount --;
            inventory.medicineAmountText.text = inventory.totalMedicineAmount.ToString();
        }

        if (inventory.totalToolsAmount >= 1)
        {
            inventory.totalToolsAmount --;
            inventory.toolsAmountText.text = inventory.totalToolsAmount.ToString();
        }
    }

    public void eventFourTalk()
    {
        if (eventNumber == 3)
        {
            if (eventFourCompleted == false && askedIfNeedHelp == true && gaveMedicine == false)
            {
                acceptButtonText.text = eventFourGiveMedicineButtonText.ToString();
                textDisplay.text = eventFourAcceptedFirst;
                eventFourTalked = true;
                needText.text = "(Need 1 medicine)";
            }

            if (eventFourCompleted == true)
            {
                acceptButtonText.text = eventOneButtonEmptyText.ToString();
                textDisplay.text = eventFourAcceptedTwice;
                sleepSystem.health -= 50;
                sleepSystem.healthAmountText.text = sleepSystem.health.ToString();
            }

            if (eventFourCompleted == false && askedIfNeedHelp == false)
            {
                acceptButtonText.text = eventFourNeedHelpButtonText.ToString();
                textDisplay.text = eventFourFirst;
                askedIfNeedHelp = true;
            }
        }
    }

    public void EventFourGiveMedicine()
    {
        if (inventory.totalMedicineAmount >= 1 && eventFourTalked == true && eventNumber == 3)
        {
            needText.text = " ";
            acceptButtonText.text = eventOneButtonEmptyText.ToString();
            textDisplay.text = eventFourAccepted;
            inventory.totalMedicineAmount--;
            inventory.medicineAmountText.text = inventory.totalMedicineAmount.ToString();
            questHints++;
            if (questHints <= 5)
            {
                hintText.text = hintText.text + ("\n") + questHintFour;
            }
            eventFourTalked = false;
            gaveMedicine = true;
        }
    }

    public void eventFiveTalk()
    {
        if (eventNumber == 4)
        {
            acceptButtonText.text = eventOneButtonEmptyText.ToString();
            textDisplay.text = eventFiveAccepted;
            leaveButtonText.text = resetLeaveButtonText.ToString();
            questHints++;
        }
    }

    public void eventFiveHide()
    {
        if (eventNumber == 4)
        {
            acceptButtonText.text = eventOneButtonEmptyText.ToString();
            textDisplay.text = eventFiveGiveUp;
            leaveButtonText.text = resetLeaveButtonText.ToString();
        }
    }

    public void EventSixGiveMedicine()
    {
        if (eventNumber == 5 && inventory.totalMedicineAmount >= 1)
        {
            needText.text = " ";
            textDisplay.text = eventSixAccepted;
            inventory.totalMedicineAmount--;
            inventory.medicineAmountText.text = inventory.totalMedicineAmount.ToString();
            questHints++;
            if (questHints <= 5)
            {
                hintText.text = hintText.text + ("\n") + questHintFive;
            }
            eventSixCompleted = true;
            foundNoteBook = true;
            acceptButtonText.text = eventOneButtonEmptyText.ToString();
        }
    }

    public void EventSevenGoToDog()
    {
        if (eventNumber == 6)
        {
            acceptButtonText.text = eventOneButtonEmptyText.ToString();

            if (dogRandom == 0)
            {
                textDisplay.text = eventSevenAccepted;
                leaveButtonText.text = resetLeaveButtonText.ToString();
                sleepSystem.health -= 20;
                sleepSystem.healthAmountText.text = sleepSystem.health.ToString();
            }
            else if (dogRandom == 1)
            {
                textDisplay.text = eventSevenFoundStash;
                leaveButtonText.text = resetLeaveButtonText.ToString();
                inventory.totalFoodAmount++;
                inventory.foodAmountText.text = inventory.totalFoodAmount.ToString();
                inventory.totalToolsAmount++;
                inventory.toolsAmountText.text = inventory.totalToolsAmount.ToString();
            }
        }
    }

    public void EventEightFix()
    {
        if (eventNumber == 7 && inventory.totalToolsAmount >= 5)
        {
            needText.text = " ";
            acceptButtonText.text = eventOneButtonEmptyText.ToString();
            textDisplay.text = eventEightAccepted;
            leaveButtonText.text = resetLeaveButtonText.ToString();
            inventory.totalToolsAmount -= 5;
            inventory.toolsAmountText.text = inventory.totalToolsAmount.ToString();
            lockBroken = false;
            lockFixed = true;
            lockBrokenDays = 0;
            expeditionSystem.fixLockUI.SetActive(false);
            lockNotBrokenDays = 0;
        }
    }

    public void EventEightFixDoor()
    {
        if (inventory.totalToolsAmount >= 5)
        {
            inventory.totalToolsAmount -= 5;
            inventory.toolsAmountText.text = inventory.totalToolsAmount.ToString();
            lockBroken = false;
            lockBrokenDays = 0;
            expeditionSystem.fixLockUI.SetActive(false);
            lockNotBrokenDays = 0;
        }
    }

    public void EventEightLeaveItAlone()
    {
        if (eventNumber == 7 && lockFixed == false)
        {
            lockBroken = true;
            leaveButtonText.text = resetLeaveButtonText.ToString();
        }
    }
}
