﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Inventory : MonoBehaviour
{
    public TextMeshProUGUI foodAmountText;
    public TextMeshProUGUI medicineAmountText;
    public TextMeshProUGUI toolsAmountText;

    public int totalFoodAmount = 0;
    public int totalMedicineAmount = 0;
    public int totalToolsAmount = 0;

    public bool hasWaterFiltration = false;
    public bool hasWeapon = false;
    public bool hasArmor = false;

}
