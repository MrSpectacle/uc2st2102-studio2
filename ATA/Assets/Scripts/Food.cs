﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Food : MonoBehaviour
{
    public GameObject foodPanel;
    public GameObject canOutline;
    public GameObject shelfOutline;
    public GameObject foodUI;

    public TextMeshProUGUI foodAmountAddedText;
    public TextMeshProUGUI foodAmountText;
    public TextMeshProUGUI hungerAddedText;
    public bool isPanelOpen = false;

    //Food amount eaten
    private int foodAmount = 0;
    //Added to food inventory
    private int newFoodInventory = 0;
    //Added to hunger status
    private int newHungerStatus = 0;

    SleepSystem sleepSystem;
    Inventory inventory;
    ExpeditionSystem expeditionSystem;
    CraftingSystem craftingSystem;
    DailyLog dailyLog;

    private void Start()
    {
        sleepSystem = GameObject.Find("Bed").GetComponent<SleepSystem>();
        inventory = GameObject.Find("GameBehavior").GetComponent<Inventory>();
        expeditionSystem = GameObject.Find("Door").GetComponent<ExpeditionSystem>();
        craftingSystem = GameObject.Find("Cardboard boxes").GetComponent<CraftingSystem>();
        dailyLog = GameObject.Find("Daily Log").GetComponent<DailyLog>();
        foodPanel.SetActive(false);
        isPanelOpen = false;
        foodAmountText.text = foodAmount.ToString();
    }

    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Escape))
        //{
        //    ClosePanel();
        //}
    }

    //private void OnMouseDown()
    //{
    //    GameObject craftingSystem = GameObject.Find("Cardboard boxes");

    //    if (craftingSystem.GetComponent<CraftingSystem>().isPanelOpen == false)
    //    {
    //        foodPanel.SetActive(true);
    //        isPanelOpen = true;
    //        newFoodInventory = inventory.totalFoodAmount;
    //        newHungerStatus = sleepSystem.hunger;
    //        hungerAddedText.text = "(" + newHungerStatus + ")".ToString();
    //        foodAmountAddedText.text = "(" + newFoodInventory + ")".ToString();
    //    }
    //}

    //public void MoreFood()
    //{
    //    if (inventory.totalFoodAmount >= 1)
    //    {
    //        if (newHungerStatus < 100 && sleepSystem.hunger < 100 &&newFoodInventory >= 1)
    //        {
    //            newHungerStatus += 40;
    //            foodAmount += 1;
    //            foodAmountText.text = foodAmount.ToString();
    //            hungerAddedText.text = "(" + newHungerStatus + ")".ToString();
    //            newFoodInventory -= 1;
    //            foodAmountAddedText.text = "(" + newFoodInventory + ")".ToString();

    //            if ((newHungerStatus) > 100)
    //            {
    //                newHungerStatus = 100;
    //                hungerAddedText.text = "(" + newHungerStatus + ")".ToString();
    //            }
    //        }
    //    }
    //}

    //public void LessFood()
    //{
    //    {
    //        if (foodAmount >= 1)
    //        {
    //            newHungerStatus -= 40;
    //            hungerAddedText.text = "(" + newHungerStatus + ")".ToString();
    //            foodAmount -= 1;
    //            foodAmountText.text = foodAmount.ToString();
    //            newFoodInventory += 1;
    //            foodAmountAddedText.text = "(" + newFoodInventory + ")".ToString();

    //            if (newHungerStatus < 0)
    //            {
    //                newHungerStatus = 0;
    //            }
    //        }
    //    }
    //}

    //public void ClosePanel()
    //{
    //    newFoodInventory = inventory.totalFoodAmount;
    //    foodAmountAddedText.text = "(" + newFoodInventory + ")".ToString();
    //    newHungerStatus = sleepSystem.hunger;
    //    hungerAddedText.text = "(" + newHungerStatus + ")".ToString();
    //    foodAmount = 0;
    //    foodAmountText.text = foodAmount .ToString();
    //    foodPanel.SetActive(false);
    //    isPanelOpen = false;
    //}

    public void Eat()
    {
        if (inventory.totalFoodAmount >= 1 && sleepSystem.hunger < 100)
        {
            inventory.totalFoodAmount--;
            inventory.foodAmountText.text = inventory.totalFoodAmount.ToString();
            sleepSystem.hunger += 40;

            if (sleepSystem.hunger >= 100)
            {
                sleepSystem.hunger = 100;
            }

            sleepSystem.hungerAmountText.text = sleepSystem.hunger.ToString();
        }
    }

    public void TakeMedicine()
    {
        if (inventory.totalMedicineAmount >= 1 && sleepSystem.health < 100)
        {
            inventory.totalMedicineAmount--;
            inventory.medicineAmountText.text = inventory.totalMedicineAmount.ToString();
            sleepSystem.health += 50;

            if (sleepSystem.health >= 100)
            {
                sleepSystem.health = 100;
            }
            sleepSystem.healthAmountText.text = sleepSystem.health.ToString();
        }
    }

    private void OnMouseEnter()
    {
        //canoutline.setactive(true);
        //shelfoutline.setactive(true);
        if (dailyLog.isPanelOpen == false && expeditionSystem.isPanelOpen == false && craftingSystem.isPanelOpen == false && sleepSystem.gameOver == false && sleepSystem.isPanelOpen == false)
        {
            foodUI.SetActive(true);
        }
    }

    private void OnMouseExit()
    {
        //canOutline.SetActive(false);
        //shelfOutline.SetActive(false);
        foodUI.SetActive(false);
    }
}
