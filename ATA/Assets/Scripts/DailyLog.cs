﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DailyLog : MonoBehaviour
{
    private int day = 1;
    private int currentDay;

    public TextMeshProUGUI dayText;
    public TextMeshProUGUI textDisplay;

    public string empty;
    [TextArea(15, 20)]
    public string dayOne;
    public string gaveFood;
    public string didNotGiveFood;
    public string didNotGiveFoodStarved;
    [TextArea(15, 20)]
    public string exploredBuilding;
    public string attacked;
    public string failedAttack;
    public string foundNotebook;
    public string lockNotFixed;
    public string burglary;

    public bool isPanelOpen = false;

    private bool starved = false;

    RandomEvents randomEvents;
    Inventory inventory;
    SleepSystem sleepSystem;

    private void Start()
    {
        isPanelOpen = true;
        textDisplay.text = dayOne;
        randomEvents = GameObject.Find("GameBehavior").GetComponent<RandomEvents>();
        inventory = GameObject.Find("GameBehavior").GetComponent<Inventory>();
        sleepSystem = GameObject.Find("Bed").GetComponent<SleepSystem>();
    }

    public void ClosePanel()
    {
        this.gameObject.SetActive(false);
        isPanelOpen = false;
    }

    public void NewDay()
    {
        isPanelOpen = true;
        textDisplay.text = empty;
        day++;
        randomEvents.lockNotBrokenDays++;
        dayText.text = ("Day ") + day.ToString();
        this.gameObject.SetActive(true);

        if (randomEvents.gaveFood == true)
        {
            textDisplay.text = gaveFood;
            randomEvents.gaveFood = false;
        }

        if (randomEvents.didNotGiveFood == true)
        {
            textDisplay.text = didNotGiveFood;
            randomEvents.didNotGiveFood = false;
            currentDay = day;
        }

        if (day == currentDay + 2 && day != 2 && starved == false)
        {
            textDisplay.text = didNotGiveFoodStarved;
            starved = true;
        }

        if (randomEvents.exploredBuilding == true)
        {
            textDisplay.text = exploredBuilding;
            randomEvents.exploredBuilding = false;
        }

        if (randomEvents.attacked == true)
        {
            textDisplay.text = attacked;
            randomEvents.attacked = false;
        }

        if (randomEvents.failedAttack == true)
        {
            textDisplay.text = failedAttack;
            randomEvents.failedAttack = false;
        }

        if (randomEvents.foundNoteBook == true)
        {
            textDisplay.text = foundNotebook;
            randomEvents.foundNoteBook = false;
        }

        if (randomEvents.lockBroken == true && randomEvents.lockBrokenDays != 5)
        {
            textDisplay.text = textDisplay.text + ("\n") + lockNotFixed;
            randomEvents.lockBrokenDays++;
        }

        if (randomEvents.lockBrokenDays >= 5)
        {
            textDisplay.text = burglary;
            randomEvents.lockBrokenDays = 0;
            inventory.totalFoodAmount = 0;
            inventory.foodAmountText.text = inventory.totalFoodAmount.ToString();
            inventory.totalMedicineAmount = 0;
            inventory.medicineAmountText.text = inventory.totalMedicineAmount.ToString();
            inventory.totalToolsAmount = 0;
            inventory.toolsAmountText.text = inventory.totalToolsAmount.ToString();
        }
    }
}
