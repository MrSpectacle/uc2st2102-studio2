﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraftingSystem : MonoBehaviour
{
    public GameObject craftingSystem;
    public GameObject notEnoughToolsText;
    public GameObject boxOutline;
    public GameObject craftUI;

    public bool isPanelOpen;

    Inventory inventory;
    SleepSystem sleepSystem;
    DailyLog dailyLog;
    ExpeditionSystem expeditionSystem;
    Food food;

    private void Start()
    {
        inventory = GameObject.Find("GameBehavior").GetComponent<Inventory>();
        sleepSystem = GameObject.Find("Bed").GetComponent<SleepSystem>();
        dailyLog = GameObject.Find("Daily Log").GetComponent<DailyLog>();
        expeditionSystem = GameObject.Find("Door").GetComponent<ExpeditionSystem>();
        food = GameObject.Find("pCylinder1").GetComponent<Food>();
    }

    public void OpenCrafting()
    {
        if (expeditionSystem.GetComponent<ExpeditionSystem>().isPanelOpen == false && sleepSystem.isPanelOpen == false)
        {
            craftingSystem.SetActive(true);
            isPanelOpen = true;
            craftUI.SetActive(false);
        }
    }

    public void ClosePanel()
    {
        craftingSystem.SetActive(false);
        isPanelOpen = false;
    }

    public void CraftWaterFiltration()
    {
        if (inventory.totalToolsAmount >= 5)
        {
            Craft();
            inventory.hasWaterFiltration = true;
        }

        else if (inventory.totalToolsAmount < 5)
        {
            notEnoughTools();
        }
    }

    public void CraftWeapon()
    {
        Craft();
        inventory.hasWeapon = true;
    }

    public void CraftAmor()
    {
        Craft();
        inventory.hasArmor = true;
    }

    void Craft()
    {
        if (inventory.totalToolsAmount >= 5)
        {
            inventory.totalToolsAmount -= 5;
            inventory.toolsAmountText.text = inventory.totalToolsAmount.ToString();
        }
    }
     
    void notEnoughTools()
    {
            notEnoughToolsText.SetActive(true);
    }

    public void Close()
    {
        notEnoughToolsText.SetActive(false);
    }

    private void OnMouseEnter()
    {
        if (dailyLog.isPanelOpen == false && expeditionSystem.GetComponent<ExpeditionSystem>().isPanelOpen == false 
            && food.GetComponent<Food>().isPanelOpen == false && isPanelOpen == false && sleepSystem.gameOver == false)
        {
            craftUI.SetActive(true);
        }
    }

    private void OnMouseExit()
    {
        craftUI.SetActive(false);
    }
}
