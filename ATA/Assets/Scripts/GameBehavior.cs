﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using Debug = UnityEngine.Debug;

public class GameBehavior : MonoBehaviour
{
    public VideoPlayer _vp;
    public GameObject Canvas;
    public GameObject StartCanvas;
    private float length;
    private float t = 0f;
    private bool isPlaying = false;

    private void Start()
    {
        if (_vp != null)
            length = (float)_vp.length;
    }

    private void Update()
    {
        if (SceneManager.GetActiveScene().buildIndex != 0) return;
        if (!isPlaying) return;

        t += Time.deltaTime;
        if (t >= length)
            LoadScene();
    }

    public void Restart()
    {
        SceneManager.LoadScene(0);
    }

    public void StartGame()
    {
        //SceneManager.LoadScene(1);
        _vp.Play();
        isPlaying = true;
        if (Canvas != null)
            Canvas.SetActive(false);

        if (StartCanvas != null)
            StartCanvas.SetActive(false);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    private void LoadScene()
    {
        SceneManager.LoadScene(1);
    }
}
