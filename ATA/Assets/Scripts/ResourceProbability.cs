﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ResourceProbability : MonoBehaviour
{
    //Chance to recieve
    public int foodMin = 0;
    public int foodMax = 3;
    public int medicineMin = 0;
    public int medicineMax = 2;
    public int toolsMin = 0;
    public int toolsMax = 4;

    private int energyRequirement;

    // UI
    private bool isTextBoxOpen = false;
    public GameObject foundFoodText;
    public GameObject foundMedicineText;
    public GameObject foundToolsText;
    public GameObject textBox;
    public GameObject notEnoughEnergyText;
    public GameObject itemAmountTextBox;
    public GameObject dontHaveObjectText;
    public GameObject randomEventText;
    public TextMeshProUGUI itemAmountText;

    //Resources recieved
    private int foodAmount = 0;
    private int medicineAmount = 0;
    private int toolsAmount = 0;

    //The amount of times have recieved the same nuber of resources consecutively
    private int foodRepeats = 0;
    private int medicineRepeats = 0;
    private int toolRepeats = 0;

    SleepSystem sleepSystem;
    Inventory inventory;
    RandomEvents randomEvents;

    private void Start()
    {
        textBox.SetActive(false);
        foundMedicineText.SetActive(false);
        foundFoodText.SetActive(false);
        notEnoughEnergyText.SetActive(false);
        itemAmountTextBox.SetActive(false);

        sleepSystem = GameObject.Find("Bed").GetComponent<SleepSystem>();
        inventory = GameObject.Find("GameBehavior").GetComponent<Inventory>();
        randomEvents = GameObject.Find("GameBehavior").GetComponent<RandomEvents>();
    }

    private void Update()
    {
        if (isTextBoxOpen == true)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                CloseTextBox();
            }
        }
    }

    public void CheckChanceFood()
    {
        randomEvents.notEnoughEnergy = false;

            energyRequirement = 50;

        if (sleepSystem.energy >= energyRequirement)
        {
            textBox.SetActive(true);
            isTextBoxOpen = true;
            itemAmountTextBox.SetActive(true);
            foundFoodText.SetActive(true);
            foodMax = foodMax++;
            foodAmount = Random.Range(foodMin, (foodMax + 1));
            foodMin = 0;
            itemAmountText.text = foodAmount.ToString();
            inventory.totalFoodAmount = inventory.totalFoodAmount + foodAmount;
            inventory.foodAmountText.text = inventory.totalFoodAmount.ToString();
            sleepSystem.energy -= 50;
            sleepSystem.energyAmountText.text = sleepSystem.energy.ToString();

            if (foodAmount == 0)
            {
                foodRepeats++;
            }
            else
            {
                foodRepeats = 0;
            }

            if (foodRepeats >= 3)
            {
                foodMin = 1;
            }
        }

        else
        {
            NotEnoughEnergy();
        }
    }

    public void CheckChanceMedicine()
    {

        energyRequirement = 100;

        if (sleepSystem.energy >= energyRequirement)
        {
            textBox.SetActive(true);
            isTextBoxOpen = true;
            itemAmountTextBox.SetActive(true);
            foundMedicineText.SetActive(true);
            medicineMax = medicineMax++;
            medicineAmount = Random.Range(medicineMin, (medicineMax + 1));
            medicineMin = 0;
            itemAmountText.text = medicineAmount.ToString();
            inventory.totalMedicineAmount = inventory.totalMedicineAmount + medicineAmount;
            inventory.medicineAmountText.text = inventory.totalMedicineAmount.ToString();
            sleepSystem.energy -= 100;
            sleepSystem.energyAmountText.text = sleepSystem.energy.ToString();

            if (medicineAmount == 0)
            {
                medicineRepeats++;
            }
            else
            {
                medicineRepeats = 0;
            }

            if (medicineRepeats >= 3)
            {
                medicineMin = 1;
            }
        }

        else
        {
            NotEnoughEnergy();
        }
    }

    public void CheckChanceTools()
    {

        energyRequirement = 50;

        if (sleepSystem.energy >= energyRequirement)
        {
            textBox.SetActive(true);
            isTextBoxOpen = true;
            itemAmountTextBox.SetActive(true);
            foundToolsText.SetActive(true);
            toolsAmount = Random.Range(toolsMin, (toolsMax + 1));
            toolsMin = 0;
            itemAmountText.text = toolsAmount.ToString();
            inventory.totalToolsAmount = inventory.totalToolsAmount + toolsAmount;
            inventory.toolsAmountText.text = inventory.totalToolsAmount.ToString();
            sleepSystem.energy -= 50;
            sleepSystem.energyAmountText.text = sleepSystem.energy.ToString();

            if (toolsAmount == 0)
            {
                toolRepeats++;
            }
            else
            {
                toolRepeats = 0;
            }

            if (toolRepeats >= 2)
            {
                toolsMin = 1;
            }
        }

        else
        {
            NotEnoughEnergy();
        }
    }

    void NotEnoughEnergy()
    {
        textBox.SetActive(true);
        isTextBoxOpen = true;
        notEnoughEnergyText.SetActive(true);
        itemAmountTextBox.SetActive(false);
        randomEvents.notEnoughEnergy = true;
    }

    public void CloseTextBox()
    {
        if (randomEvents.notEnoughEnergy == false)
        {
            randomEvents.RandomEvent();
        }

        if (randomEvents.randomEventCheck == false)
        {
            textBox.SetActive(false);
            foundMedicineText.SetActive(false);
            foundFoodText.SetActive(false);
            foundToolsText.SetActive(false);
            notEnoughEnergyText.SetActive(false);
            itemAmountTextBox.SetActive(false);
            dontHaveObjectText.SetActive(false);
            randomEventText.SetActive(false);
            isTextBoxOpen = false;
        }

        if (randomEvents.randomEventCheck == true)
        {
            foundMedicineText.SetActive(false);
            foundFoodText.SetActive(false);
            foundToolsText.SetActive(false);
            notEnoughEnergyText.SetActive(false);
            itemAmountTextBox.SetActive(false);
            dontHaveObjectText.SetActive(false);
        }

    }

    public void CloseRandomEvent()
    {
        if (randomEvents.randomEventCheck == true)
        {
            textBox.SetActive(false);
            foundMedicineText.SetActive(false);
            foundFoodText.SetActive(false);
            foundToolsText.SetActive(false);
            notEnoughEnergyText.SetActive(false);
            itemAmountTextBox.SetActive(false);
            dontHaveObjectText.SetActive(false);
            randomEventText.SetActive(false);
            isTextBoxOpen = false;
            randomEvents.randomEventCheck = false;
            randomEvents.RandomEventFirstButton.SetActive(false);
            randomEvents.RandomEventOkayButton.SetActive(false);
            randomEvents.RandomEventRefuseButton.SetActive(false);
            randomEvents.okayButton.SetActive(true);
            randomEvents.eventOneStarted = false;
        }
    }
}