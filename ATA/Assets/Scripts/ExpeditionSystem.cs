﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ExpeditionSystem : MonoBehaviour
{
    public GameObject expeditionSystem;
    public GameObject doorOutline;
    public bool isPanelOpen = false;

    bool expeditionSystemIsOpen = false;

    public TextMeshProUGUI energyText;
    public GameObject energyAmountText;
    public GameObject hungerAmountText;
    public GameObject healthAmountText;
    public GameObject questTextBox;
    public GameObject exploreUI;
    public GameObject fixLockUI;

    SleepSystem sleepSystem;
    DailyLog dailyLog;
    Food food;
    CraftingSystem craftingSystem;
    RandomEvents randomEvents;

    private void Start()
    {
        expeditionSystem.SetActive(false);
        sleepSystem = GameObject.Find("Bed").GetComponent<SleepSystem>();
        dailyLog = GameObject.Find("Daily Log").GetComponent<DailyLog>();
        food = GameObject.Find("pCylinder1").GetComponent<Food>();
        craftingSystem = GameObject.Find("Cardboard boxes").GetComponent<CraftingSystem>();
        randomEvents = GameObject.Find("GameBehavior").GetComponent<RandomEvents>();
    }

    private void Update()
    {
        if (expeditionSystemIsOpen == true)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                CloseExpeditionSystem();
            }
        }
    }

    public void OpenExpeditionSystem()
    {

        if (sleepSystem.GetComponent<SleepSystem>().isPanelOpen == false && food.GetComponent<Food>().isPanelOpen == false && craftingSystem.GetComponent<CraftingSystem>().isPanelOpen == false)
        {
            expeditionSystem.SetActive(true);
            expeditionSystemIsOpen = true;
            isPanelOpen = true;
            energyText.text = "".ToString();
            energyText.color = Color.black;
            energyAmountText.SetActive(false);
            hungerAmountText.SetActive(false);
            healthAmountText.SetActive(false);
            exploreUI.SetActive(false);
            fixLockUI.SetActive(false);
        }
    }

    public void CloseExpeditionSystem()
    {
        expeditionSystem.SetActive(false);
        expeditionSystemIsOpen = false;
        questTextBox.SetActive(false);
        isPanelOpen = false;
        energyText.text = "       Energy<br>       Hunger<br>       Health".ToString();
        energyText.color = Color.white;
        energyAmountText.SetActive(true);
        hungerAmountText.SetActive(true);
        healthAmountText.SetActive(true);
    }

    private void OnMouseEnter()
    {
        if (dailyLog.isPanelOpen == false && food.GetComponent<Food>().isPanelOpen == false && isPanelOpen == false 
            && sleepSystem.gameOver == false && craftingSystem.isPanelOpen == false)
        {
            if (randomEvents.lockBroken == false)
            {
                exploreUI.SetActive(true);
            }

            else if (randomEvents.lockBroken == true)
            {
                fixLockUI.SetActive(true);
            }
        }
    }

    private void OnMouseExit()
    {
        exploreUI.SetActive(false);
        fixLockUI.SetActive(false);
    }
}